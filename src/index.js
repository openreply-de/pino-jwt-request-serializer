'use strict';

const jwt = require('jsonwebtoken');
const jmespath = require('jmespath');
const serializers = require('pino-std-serializers');

module.exports = query =>
  serializers.wrapRequestSerializer(req => Object.assign({}, req, jmespath.search(jwt.decode((req.headers['authorization'] || '').substr(7)) || {}, query)));
